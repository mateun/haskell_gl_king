# OpenGL based game written in Haskell

This is a small game to learn how to use the haskell gl binding. 

Beware: currently we need to copy the mingw64/bin/SDL2.dll into the executable directory under 
"dist-newstyle/.."


Currently we can only see stdout output on windows from the msys2 window. 
run 

```
cabal build
```

and then go the executable file (see above) and run gl-game.exe from there. 
