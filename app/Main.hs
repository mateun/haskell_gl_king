{-# LANGUAGE CPP #-}
{-# LANGUAGE OverloadedStrings #-}
import SDL
import SDL.Raw.Basic
import Control.Monad (unless)
import Linear 
import System.IO
import Foreign.C.String
import Data.Word

data Player = Player { pos:: V2 Float }
data RenderState = RS { col : V4 Word8 }

main :: IO () 
main = do 
  msg <- newCString "starting"
  SDL.Raw.Basic.log msg
  hFlush stdout
  SDL.initialize [SDL.InitVideo]
  SDL.HintRenderScaleQuality $= SDL.ScaleLinear
  window <- createWindow "demo" defaultWindow 
  renderer <- createRenderer window (-1) defaultRenderer

  appLoop renderer $ Player $ V2 10 0

  SDL.destroyWindow window
  SDL.quit



update :: Player -> IO(V2 Float)
update player = do
	return  (pos player + V2 0.1 0)
	
changeColor :: Int -> Int
changeColor old = let newColor = old + 1
                  in if newColor > 255
									   then 0
										 else newColor

col :: RenderState -> V4 Word8
col (RS c)  = V4 x 255 255 255
              where x = c
							

appLoop :: Renderer -> Player -> RenderState -> IO() 
appLoop renderer player rs = do 
  events <- pollEvents
  let { quit = elem QuitEvent $ map eventPayload events }
  rendererDrawColor renderer $= col 
  clear renderer
  update player 
  present renderer
  unless quit (appLoop renderer player)

